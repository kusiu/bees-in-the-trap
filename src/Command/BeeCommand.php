<?php

namespace App\Command;

use App\Service\HiveService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class BeeCommand extends Command
{
    const QUEEN_BEES = 1;
    const WORKER_BEES = 5;
    const DRONE_BEES = 8;

    protected static $defaultName = 'bees:play';

    protected function configure()
    {
        $this
            ->setDescription('Kill bees in the trap')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $hiveService = new HiveService(self::QUEEN_BEES, self::WORKER_BEES, self::DRONE_BEES);

        $progressBar = new ProgressBar($output, count($hiveService->getBees()));
        do {
            $io->ask('Press enter to hit');

            $bee = $hiveService->getRandomBee();
            $hiveService->play($bee);

            if ($bee->isDead()) {
                $io->text('Direct Hit! You took '.$bee->getDeductedPoints().' hit points from a '.$bee->getName().'. The bee is dead. ');
                $progressBar->advance();
            } else {
                $io->text('Direct Hit! You took '.$bee->getDeductedPoints().' hit points from a '.$bee->getName().' ('.$bee->getLifespan().' points left).');
            }


        } while (count($hiveService->getBees()) > 0);

        $io->success('Game over. ' . $hiveService->getHits().' hits were needed to destroy the hive.');
    }
}