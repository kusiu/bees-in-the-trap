<?php

namespace App\Service;

use App\Model\Bee;
use App\Model\DroneBee;
use App\Model\QueenBee;
use App\Model\WorkerBee;

class HiveService
{
    private $bees = [];
    private $hits = 0;

    public function __construct(int $queenAmount, int $workerAmount, int $droneAmount)
    {
        // Add Queen Bee
        $queenBee = new QueenBee(0);
        $this->bees[$queenBee->getIndex()] = $queenBee;

        // Add Worker Bees
        for ($i=0; $i<$workerAmount; $i++) {
            $workerBee = new WorkerBee($i + $queenAmount);
            $this->bees[$workerBee->getIndex()] = $workerBee;
        }

        // Add Worker Bees
        for ($i=0; $i<$droneAmount; $i++) {
            $droneBee = new DroneBee($i + $queenAmount + $droneAmount);
            $this->bees[$droneBee->getIndex()] = $droneBee;
        }
    }

    public function play(Bee $bee)
    {
        $this->hits++;

        $bee->hit();

        if ($bee->isDead()) {
            // If Queen is dead then all remaining alive Bees automatically run out of hit points
            if ($bee instanceof QueenBee) {
                array_splice($this->bees, 0);
            } else {
                unset($this->bees[$bee->getIndex()]);
            }
        }
    }

    public function getRandomBee(): Bee
    {
        $key = array_rand($this->bees, 1);
        /** @var Bee $bee */
        return $this->bees[$key];
    }

    public function getHits(): int
    {
        return $this->hits;
    }

    public function getBees(): array
    {
        return $this->bees;
    }
}