<?php

namespace App\Model;

class DroneBee extends Bee
{
    protected $deductedPoints = 12;
    protected $lifespan = 50;
    protected $name = 'Drone Bee';

    public function __construct(int $index)
    {
        parent::__construct($index);
    }
}