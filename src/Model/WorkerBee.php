<?php

namespace App\Model;

class WorkerBee extends Bee
{
    protected $deductedPoints = 10;
    protected $lifespan = 75;
    protected $name = 'Worker Bee';

    public function __construct(int $index)
    {
        parent::__construct($index);
    }
}