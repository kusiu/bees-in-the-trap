<?php

namespace App\Model;

abstract class Bee
{
    protected $deductedPoints;
    protected $lifespan;
    protected $name;
    protected $index;

    public function __construct(int $index)
    {
        $this->index = $index;
    }

    public function getDeductedPoints(): int
    {
        return $this->deductedPoints;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getLifespan(): int
    {
        return $this->lifespan;
    }

    public function isDead(): bool
    {
        if ($this->lifespan <= 0) {
            return true;
        }

        return false;
    }

    public function hit()
    {
        $this->lifespan -= $this->deductedPoints;
    }

    public function getIndex(): int
    {
        return $this->index;
    }

    public function setIndex($index): void
    {
        $this->index = $index;
    }
}