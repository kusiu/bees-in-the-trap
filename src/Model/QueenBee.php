<?php

namespace App\Model;

class QueenBee extends Bee
{
    protected $deductedPoints = 8;
    protected $lifespan = 100;
    protected $name = 'Queen Bee';

    public function __construct(int $index)
    {
        parent::__construct($index);
    }
}