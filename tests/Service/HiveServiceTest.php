<?php

namespace App\Tests\Model;

use App\Model\QueenBee;
use App\Service\HiveService;
use PHPUnit\Framework\TestCase;

class HiveServiceTest extends TestCase
{
    /**
     * @test
     */
    public function checkInitialNumberOfBees()
    {
        $hiveService = new HiveService(1,2,3);
        $this->assertCount(6, $hiveService->getBees());
    }

    /**
     * @test
     */
    public function checkIfAllBeesAreKilledIfQueenIsDeath()
    {
        $hiveService = new HiveService(1,1,1);
        $bee = new QueenBee(1);

        for ($i = 0; $i < 13; $i++) {
            $hiveService->play($bee);
        }

        $this->assertTrue($bee->isDead());
        $this->assertCount(0, $hiveService->getBees());
    }
}