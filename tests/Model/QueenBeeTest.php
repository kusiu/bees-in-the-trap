<?php

namespace App\Tests\Model;

use PHPUnit\Framework\TestCase;

class QueenBeeTest extends TestCase
{
    /**
     * @test
     */
    public function testHit()
    {
        $bee = new \App\Model\QueenBee(1);
        $this->assertEquals(100, $bee->getLifespan());
        $bee->hit();
        $this->assertEquals(92, $bee->getLifespan());
        $this->assertFalse($bee->isDead());
    }
}