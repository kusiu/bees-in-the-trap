<?php

namespace App\Tests\Model;

use PHPUnit\Framework\TestCase;

class DroneBeeTest extends TestCase
{
    /**
     * @test
     */
    public function testHit()
    {
        $bee = new \App\Model\DroneBee(1);
        $this->assertEquals(50, $bee->getLifespan());
        $bee->hit();
        $this->assertEquals(38, $bee->getLifespan());
        $this->assertFalse($bee->isDead());
    }
}