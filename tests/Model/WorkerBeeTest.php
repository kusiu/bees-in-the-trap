<?php

namespace App\Tests\Model;

use PHPUnit\Framework\TestCase;

class WorkerBeeTest extends TestCase
{
    /**
     * @test
     */
    public function testHit()
    {
        $bee = new \App\Model\WorkerBee(1);
        $this->assertEquals(75, $bee->getLifespan());
        $bee->hit();
        $this->assertEquals(65, $bee->getLifespan());
        $this->assertFalse($bee->isDead());
    }
}